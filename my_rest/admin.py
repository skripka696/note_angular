from django.contrib import admin
from my_rest.models import  Color, Category,Label, Note, Media_Note, Comment
from django_mptt_admin.admin import DjangoMpttAdmin
from django.contrib.auth.models import User


class CategoryAdmin(DjangoMpttAdmin):
    list_display = ('category_name','parent')


class NoteAdmin(admin.ModelAdmin):

    list_display = ['title','content']


admin.site.register(Color)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Label)
admin.site.register(Note, NoteAdmin)
admin.site.register(Media_Note)
admin.site.register(Comment)


from django.db import models
from colorful.fields import RGBColorField
from django.contrib.auth.models import User
import mptt
# from mptt.fields import TreeForeignKey
#from mptt.models import MPTTModel

class Color(models.Model):
    code = RGBColorField()
    
    def __unicode__(self):
        return '{0}'.format( self.code)


class Category(models.Model):
    category_name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', blank=True, null=True,
                               verbose_name="Parent", related_name='Child')

    def __unicode__(self):
        return '{0}'.format(self.category_name)

mptt.register(Category,)

class Label(models.Model):
    label_name = models.CharField(max_length=20) 

    def __unicode__(self):
        return '{0}'.format(self.label_name)


class Media_Note(models.Model):
    my_file = models.FileField(upload_to = 'media/', default="media/note.jpg")

    def __unicode__(self):
        return '{0}'.format(self.my_file)


class Note(models.Model):
    title = models.CharField(max_length=30)
    content = models.CharField(max_length=255)
    color = models.ForeignKey(Color, blank=True , null=True, related_name='color')
    note_category = models.ManyToManyField(Category)
    note_label = models.ManyToManyField(Label)
    note_media = models.ManyToManyField(Media_Note)
    user = models.ForeignKey(User)

 
    def __unicode__(self):
        return '{0} - {1}'.format(self.title, self.content)


class Comment(models.Model):
    content = models.CharField(max_length=255)
    user_comment = models.ForeignKey(User)
    note_comment = models.ForeignKey(Note, blank=True , null=True, related_name='note_comment')

    def __unicode__(self):
        return '{0} {1} {2}'.format(self.content, self.user_comment, self.note_comment)

  



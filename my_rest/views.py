from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response

from models import Note
from my_rest.serializers import  NotePostSerializer,ColorSerializer, LabelSerializer,CategorySerializer, MediaSerializer, UserSerializer,UserPostSerializer, NoteGetSerializer,NoteUserSerializer,CategoryAddSerializer, CommentGetSerializer, CommentPostSerializer,CommentSerializer, SerialUser, UserSerializer
from models import Note, Category, Color, Label, Media_Note, Comment
from rest_framework import mixins
from rest_framework import generics
# from rest_framework import viewsets, permissions, status
from rest_framework import viewsets,status
from rest_framework.decorators import detail_route
from rest_framework import permissions
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from rest_framework import authentication, serializers
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

class NoteViewSet(viewsets.ModelViewSet):
    """
    Returns a list of notes.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/
    
    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/

    For more details about all media [see here][not].
    [not]: http://127.0.0.1:8000/api/media/

    For more details about all user [see here][not].
    [not]: http://127.0.0.1:8000/api/user/

    For more details about all comment [see here][com].
    [com]: http://127.0.0.1:8000/api/comment/
    """
    queryset = Note.objects.all()
    serializer_class = NotePostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request):
        queryset = Note.objects.all()
        serializer = NoteGetSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Note.objects.get(pk=pk)
        serializer = NoteGetSerializer(queryset, many=False)
        return Response(serializer.data)

    def create(self, request):
        request.data['user'] = request.user.pk
        serializer = NoteUserSerializer(data=request.data)
        print serializer
        if serializer.is_valid():
            serializer.save()
            return Response( status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
   

class ColorViewSet(viewsets.ModelViewSet):
    """
    Returns a list of color.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/
    
    For more details about all notes [see here][not].
    [not]: http://127.0.0.1:8000/api/note/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/

    For more details about all media [see here][not].
    [not]: http://127.0.0.1:8000/api/media/

    For more details about all user [see here][not].
    [not]: http://127.0.0.1:8000/api/user/

    For more details about all comment [see here][com].
    [com]: http://127.0.0.1:8000/api/comment/
    """
    queryset = Color.objects.all()
    serializer_class = ColorSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class CategoryViewSet(viewsets.ModelViewSet):
    """
    Returns a list of category.
    Edit, delete and add new ones.

    For more details about all note [see here][not].
    [not]: http://127.0.0.1:8000/api/note/
    
    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/

    For more details about all media [see here][med].
    [med]: http://127.0.0.1:8000/api/media/

    For more details about all user [see here][us].
    [us]: http://127.0.0.1:8000/api/user/

    For more details about all comment [see here][com].
    [com]: http://127.0.0.1:8000/api/comment/
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request):
        queryset = Category.objects.all()
        serializer = CategoryAddSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Category.objects.get(pk=pk)
        serializer = CategoryAddSerializer(queryset, many=True)
        return Response(serializer.data)


class LabelViewSet(viewsets.ModelViewSet):
    """
    Returns a list of label.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/
    
    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all note [see here][not].
    [not]: http://127.0.0.1:8000/api/note/

    For more details about all media [see here][med].
    [med]: http://127.0.0.1:8000/api/media/

    For more details about all user [see here][us].
    [us]: http://127.0.0.1:8000/api/user/

    For more details about all comment [see here][com].
    [com]: http://127.0.0.1:8000/api/comment/
    """
    queryset = Label.objects.all()
    serializer_class = LabelSerializer    
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class MediaViewSet(viewsets.ModelViewSet):
    """
    Returns a list of media.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/

    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all note [see here][not].
    [not]: http://127.0.0.1:8000/api/note/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/

    For more details about all user [see here][us].
    [us]: http://127.0.0.1:8000/api/user/

    For more details about all comment [see here][com].
    [com]: http://127.0.0.1:8000/api/comment/
    """
    queryset = Media_Note.objects.all()
    serializer_class = MediaSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class UserViewSet(viewsets.ModelViewSet):
    """
    Returns a list of users.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/

    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all note [see here][not].
    [not]: http://127.0.0.1:8000/api/note/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/

    For more details about all media [see here][med].
    [med]: http://127.0.0.1:8000/api/media/

    For more details about all comment [see here][com].
    [com]: http://127.0.0.1:8000/api/comment/
    """
    queryset = User.objects.all()
    serializer_class = UserPostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class NoteList(TemplateView):
    template_name = 'my_rest/note.html'
    def get_context_data(self, **kwargs):
        context = super(NoteList, self).get_context_data(**kwargs)
        return context

class CommentViewSet(viewsets.ModelViewSet):
    """
    Returns a list of users.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/

    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all note [see here][not].
    [not]: http://127.0.0.1:8000/api/note/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/

    For more details about all media [see here][med].
    [med]: http://127.0.0.1:8000/api/media/

    For more details about all user [see here][us].
    [us]: http://127.0.0.1:8000/api/user/
    """
    queryset = Comment.objects.all()
    serializer_class = CommentPostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request):
        queryset = Comment.objects.all()
        serializer = CommentGetSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Comment.objects.filter(note_comment=pk)
        serializer = CommentGetSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        request.data['user_comment'] = request.user.pk
        serializer = CommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response( status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuthenticationApi(APIView):
    serializer_class = SerialUser
    model = User

    def post(self, request, *args, **kwargs):
        user = authenticate(username=request.data.get("username"), password=request.data.get("password"))
        login(request, user)
        return HttpResponseRedirect('/')

    def delete(self, request, *args, **kwargs):
        """Logout from system"""
        logout(request)
        return Response("ok")


class Regist(APIView):

    queryset = User.objects.all()
    serializer_class = UserPostSerializer

    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def post(self, request, format=None):
        content = {
            'user': unicode(request.user),
            'auth': unicode(request.auth),
        }
        return Response(content)







from django import forms
from models import Note

from django.contrib.auth.models import User


class NoteForm(forms.ModelForm):


    class Meta:
        model = Note
        fields = '__all__'



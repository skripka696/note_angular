
from django.forms import widgets
from rest_framework import serializers
from my_rest.models import Note, Color, Category, Label, Media_Note, Comment
from django.contrib.auth.models import User



class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)


    # class Meta:
    #     model = User
    #     fields = ('pk', 'username', 'password')
  

class ColorSerializer(serializers.ModelSerializer):
    # color = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:   
      model = Color
      fields = ('pk','code')
          

class LabelSerializer(serializers.ModelSerializer):
    
    #label_name = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta: 
        model = Label
        fields = ('pk','label_name',)
   

class MediaSerializer(serializers.ModelSerializer):
   # my_file = serializers.FileField(max_length=255)
   
    class Meta: 
        model = Media_Note
        fields = ['pk','my_file',]



class CategorySerializer(serializers.ModelSerializer):

    category_name = serializers.CharField(max_length=255)
    # Child = serializers.StringRelatedField(many=True)

    class Meta: 
        model = Category
        fields = ['pk','category_name']

class CategoryAddSerializer(serializers.ModelSerializer):

    category_name = serializers.CharField(max_length=255)
    Child = serializers.StringRelatedField(many=True)

    class Meta:
        model = Category
        fields = ['pk','category_name', 'Child']
        
# class NoteSerializer(serializers.Serializer):
#   pk = serializers.IntegerField(read_only=True)
#   title = serializers.CharField(required=False, max_length=30)


class CommentGetSerializer(serializers.ModelSerializer):
    content = serializers.CharField(max_length=255)
    user_comment = UserSerializer(required=False)
    # note_comment = NoteGetSerializer(required=False)

    class Meta:
        model = Comment
        fields = ['pk','content','user_comment', 'note_comment']

class CommentPostSerializer(serializers.ModelSerializer):
    # content = serializers.CharField(max_length=255)
    # user_comment = UserSerializer(required=False)
    # note_comment = NoteGetSerializer(required=False)

    class Meta:
        model = Comment
        fields = ['pk','content', 'note_comment']

class CommentSerializer(serializers.ModelSerializer):
    # content = serializers.CharField(max_length=255)
    # user_comment = UserSerializer(required=False)
    # note_comment = NoteGetSerializer(required=False)

    class Meta:
        model = Comment
        fields = ['pk','content', 'user_comment', 'note_comment']

class NotePostSerializer(serializers.ModelSerializer):

    pk = serializers.IntegerField(read_only=True)
    #title = serializers.CharField(required=False, max_length=30)
    # user = UserSerializer(required=False)
    content = serializers.CharField(max_length=200)
    # note_media = MediaSerializer(many=True)
    # note_category = CategorySerializer(many=True)
    # note_label = LabelSerializer(many=True)
    # color = ColorSerializer(required=False, read_only=True)


    class Meta:
        model = Note
        fields = ['pk','title','content', 'color']


class NoteGetSerializer(serializers.ModelSerializer):

    pk = serializers.IntegerField(read_only=True)
    #title = serializers.CharField(required=False, max_length=30)
    user = UserSerializer(required=False)
    content = serializers.CharField(max_length=200, required=False)
    note_media = MediaSerializer(many=True, required=False)
    note_category = CategorySerializer(many=True, required=False)
    note_label = LabelSerializer(many=True, required=False)
    color = ColorSerializer(required=False, read_only=True)
    note_comment = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Note
        fields = ['pk','title','content','color', 'user','note_media', 'note_category','note_label', 'note_comment']


class NoteUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = Note
        fields = ['pk','title','content','color', 'user']


class SerialUser(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['pk', 'username', 'password']


class UserPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'password')
        write_only_fields = ('password',)
        read_only_field = ('id',)


    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            # email=validated_data['email'],
            # first_name=validated_data['first_name'],
            # last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

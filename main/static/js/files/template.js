var noteApp = angular.module('noteApp', [
	'ui.router',
	'ngRoute',
	'ngResource',
	'restangular',
//	'pascalprecht.translate',
//	'ngCookies',
//	'ui.bootstrap',
//	'autoActive',
//	'ngAnimate',
//    'ngStorage',
//    'ngSanitize',
//    'ngTouch',
//    'afOAuth2',
]);


noteApp.controller('AuthController', function($scope, $http, $q, $location) {

	$scope.submitAuth = function() {
			var d = $q.defer();

			$http({
				method: 'POST',
				url: '/api/login/',
				data: {username: $scope.username,
					password: $scope.password
				},
			}).success(function (response) {
				console.log($scope.username);
				 window.location = "/"
			}).error(function (e) {
				$scope.error = "not right";
				scope.$on('$locationChangeSuccess', $scope.error);
			});
	};
});


// Get list of notes from rest framework
noteApp.factory('NoteService', function($resource){
	return $resource('/api/note/?format=json');
});

// List of notes
noteApp.controller('NoteController',
	function ($scope, NoteService) {
		NoteService.query(function(result) {
			$scope.noteList=result;
		});
});

 //Get note detail from rest framework
noteApp.factory('NoteIdService', function($resource){
	return $resource('/api/note/:pk/?format=json', {pk:'@pk'});

});

noteApp.factory('CommentIdService', function($resource){
	return $resource('/api/comment/:pk/?format=json', {pk:'@pk'});
});

 //Comment detail
noteApp.controller('NoteIdController',
	function ($location, $route, $scope, $http, $routeParams, NoteIdService, CommentIdService, LabelService) {

		NoteIdService.get({pk: $route.current.params.pk}, function (result) {
			$scope.note = result;
		});

		CommentIdService.query({pk: $route.current.params.pk}, function (result) {
			$scope.comment = result;
			console.log($scope.comment);

		});

		$scope.submitComment = function(){
			var data= {content: $scope.selected_content,
		 				note_comment:$scope.note.pk,

		 			}
//		 	console.log($location);
//			console.log(AuthUserProvider.username);
		 	$http({
   				method: 'POST',
    			url: '/api/comment/',
    			data: data,
    			headers: {'Content-Type': 'application/json;charset=utf-8'}
			});
			$scope.comment.push(
			CommentIdService.query({pk: $route.current.params.pk}, function (result) {
				$scope.comment = result;
					console.log($scope.comment);

				})

			);
		};

});

 //comment detail
noteApp.controller('CommentIdController',
	function ($location, $route, $scope, $routeParams, CommentIdService) {

			CommentIdService.get({pk: $route.current.params.pk}, function (result) {
				$scope.comment = result;

		});
});

// Get list of category from rest framework
noteApp.factory('CategoryService', function($resource){
	return $resource('/api/category/?format=json');
});

// List of category
noteApp.controller('CategoryController',
	function ($scope, CategoryService) {
		CategoryService.query(function(result) {
			$scope.categoryList=result;
		});
});

// Get list of label from rest framework
noteApp.factory('LabelService', function($resource){
	return $resource('/api/label/?format=json');
});

// List of label
noteApp.controller('LabelController',
	function ($scope, LabelService) {
		LabelService.query(function(result) {
			$scope.labelList=result;
		});
});

// Get list of color from rest framework
noteApp.factory('ColorService', function($resource){
	return $resource('/api/color/?format=json');
});

// List of color
noteApp.controller('ColorController',
	function ($scope, ColorService) {
		ColorService.query(function(result) {
			$scope.colorList=result;
		});
});

// Get list of media from rest framework
noteApp.factory('MediaService', function($resource){
	return $resource('/api/media/?format=json');
});

// List of media
noteApp.controller('MediaController',
	function ($scope, MediaService) {
		MediaService.query(function(result) {
			$scope.mediaList=result;
		});
});

// Get list of user from rest framework
noteApp.factory('UserService', function($resource){
	return $resource('/api/user/?format=json');
});

// List of user
noteApp.controller('UserController',
	function ($scope, UserService) {
		UserService.query(function(result) {
			$scope.userList=result;
		});
});


// Get list of comments from rest framework
noteApp.factory('CommentService', function($resource){
	return $resource('/api/comment/?format=json');
});

// List of comments
noteApp.controller('CommentController',
	function ($scope, CommentService) {
		CommentService.query(function(result) {
			$scope.commentList=result;
		});
});

//new Note
noteApp.factory("Post", function($resource) {
  return $resource('/api/note/?format=json',
  				   '/api/category/?format=json',
  				   '/api/color/?format=json',
  				   '/api/label/?format=json',
  				   '/api/media/?format=json',
  				   '/api/user/?format=json');
});

//new note
noteApp.controller('NoteNewController', function($scope, $http,$q, Post, CategoryService, LabelService, ColorService, MediaService) {

		CategoryService.query(function(result) {
			$scope.categoryList=result;
		});

		ColorService.query(function(result) {
			$scope.colorList=result;
		});

		LabelService.query(function(result) {
			$scope.labelList=result;
		});

		MediaService.query(function(result) {
			$scope.mediaList=result;
		});
		$scope.submitNote = function(){
		 	var data= {title: $scope.selected_title,
		 			content: $scope.selected_content,
		 			category:$scope.selected_category,
		 			color:$scope.selected_color.pk,
		 			label:$scope.selected_label,
		 			my_file: $scope.selected_media,
		 		}

		 	$http({
   				method: 'POST',
    			url: '/api/note/',
    			data: data,
    			headers: {'Content-Type': 'application/json;charset=utf-8'}
			});
		};
});

//new label
noteApp.factory("PostLabel", function($resource) {
  return $resource('/api/category/?format=json');
});

noteApp.controller('LabelNewController', function($scope, $http,$q, PostLabel, LabelService) {

		LabelService.query(function(result) {
			//console.log(result)
			$scope.labelList=result;
		});

		$scope.submitLabel = function(){
		 	var data= {label_name:$scope.selected_label_name}
		 	//console.log(data)
		 	$http({
   				method: 'POST',
    			url: '/api/label/',
    			data: data,
    			headers: {'Content-Type': 'application/json;charset=utf-8'}
			});
		};
});


//new category
noteApp.factory("PostCategory", function($resource) {
  return $resource('/api/category/?format=json');
});

noteApp.controller('CategoryNewController', function($scope, $http,$q, PostCategory, CategoryService) {

		CategoryService.query(function(result) {
			$scope.categoryList=result;
		});

		$scope.submitCategory = function(){
		 	var data= {category_name:$scope.selected_category_name}
		 	$http({
   				method: 'POST',
    			url: '/api/category/',
    			data: data,
    			headers: {'Content-Type': 'application/json;charset=utf-8'}
			});
		};
});

//new comment
noteApp.factory("PostComment", function($resource) {
  return $resource('/api/comment/?format=json');
});

noteApp.controller('CommentNewController', function($scope, $http,$q, PostComment, CommentService) {

		CommentService.query(function(result) {
			$scope.commentList=result;
		});

		$scope.submitComment = function(){
		 	var data= {category_name:$scope.selected_category_name}
		 	$http({
   				method: 'POST',
    			url: '/api/comment/',
    			data: data,
    			headers: {'Content-Type': 'application/json;charset=utf-8'}
			});
		};
});

//new user
noteApp.factory("PostUser", function($resource) {
  return $resource('/api/user/?format=json');
});

noteApp.controller('UserNewController', function($scope, $http,$q, PostUser, UserService) {

		UserService.query(function(result) {
			//console.log(result)
			$scope.userList=result;
		});

		$scope.submitUser = function(){

		 	var data= {username:$scope.selected_username,
		 				password:$scope.selected_password,
		 				password2:$scope.selected_password2}
		 	//console.log(data)
		 	$http({
   				method: 'POST',
    			url: '/api/user/',
    			data: data,
    			headers: {'Content-Type': 'application/json;charset=utf-8'}
			});
		};
});

noteApp.config(function($routeProvider, $locationProvider) {


	$routeProvider.when('/note', {
			templateUrl: '../static/html/note_list.html'
		});
	$routeProvider.when('/note/comment/', {
			templateUrl: '../static/html/comment_list.html'
		});
	$routeProvider.when('/note/new_comment/', {
			templateUrl: '../static/html/new_comment.html'
		});
	$routeProvider.otherwise({ redirectTo: '/note' });
	$routeProvider.when('/note/category/', {
			templateUrl: '../static/html/category_list.html'
		});
	$routeProvider.when('/note/new_note/', {
			templateUrl: '../static/html/new_note.html'
		});
	$routeProvider.when('/note/new_category/', {
			templateUrl: '../static/html/new_category.html'
		});
	$routeProvider.when('/note/new_label/', {
			templateUrl: '../static/html/new_label.html'
		});
	$routeProvider.when('/note/new_user/', {
			templateUrl: '../static/html/regist.html'
		});
	$routeProvider.when('/', {
			templateUrl: '../static/html/login.html'
		});
	$routeProvider.when('/note/:pk/', {
			templateUrl: '../static/html/note_list_id.html'
		});
	$routeProvider.when('/note/comment/:pk/', {
			templateUrl: '../static/html/comment_list_id.html'
		});

});


noteApp.config(function($httpProvider) {
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
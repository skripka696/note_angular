from django import forms
from my_rest.models import Note, Category, Label


class NoteForm(forms.ModelForm):
	class Meta:
		model = Note
		fields = '__all__'


class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = '__all__'


class LabelForm(forms.ModelForm):
	class Meta:
		model = Label
		fields = ('label_name',)


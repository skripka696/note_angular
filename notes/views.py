from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from my_rest.models import Note, Media_Note, Color, Label, Category
from django.contrib.auth.models import User
from my_rest.forms import NoteForm
from my_rest.serializers import NotePostSerializer
from notes.forms import CategoryForm, LabelForm
from django.views.generic.edit import FormView, View, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, DetailView, ListView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator



class SaveView(TemplateView):
    model = Note
    template_name = 'notes/save.html'


class Regist(FormView):
    form_class = UserCreationForm
    success_url = '/get_save/'
    template_name = 'notes/login.html'

    def form_valid(self,form):
        self.object = form.save()
        return super(Regist, self).form_valid(form)


class Login(FormView):
    form_class = AuthenticationForm
    success_url = '/get_list/'
    template_name = 'notes/login.html'

    def form_valid(self,form):
        user = form.get_user()
        login(self.request, user)
        return super(Login, self).form_valid(form)


class LogOut(View):
    success_url = '/'
    template_name = 'notes/base.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('#/')


class NewNote(FormView):
    form_class = NoteForm
    success_url = '/get_save/'
    template_name = 'notes/new_note.html'

    def form_valid(self, form):
        form.save()
        return super(NewNote, self).form_valid(form)


class NewCategory(FormView):
    form_class = CategoryForm
    success_url = '/get_save/'
    template_name = 'notes/new_category.html'

    def form_valid(self, form):
        form.save()
        return super(NewCategory, self).form_valid(form)


class NewLabel(FormView):
    form_class = LabelForm
    success_url = '/get_save/'
    template_name = 'notes/new_label.html'

    def form_valid(self, form):
        form.save()
        return super(NewLabel, self).form_valid(form)


class GetNoteView(ListView):
    model = Note
    template_name = 'notes/note_view_list.html'

    def get(self, request,*args,**kwargs):
        #if request.user.is_authenticated():
        user = request.user
        note = user.note_set.all()
        return render(request, self.template_name, {'note_list': note})

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(GetNoteView, self).dispatch(*args, **kwargs)

class GetNoteIdView(TemplateView):
    # model = Note
    template_name = 'notes/note_id_view_list.html'


    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(GetNoteIdView, self).dispatch(*args, **kwargs)

class GetCategoryView(ListView):
    model = Category
    template_name = 'notes/category.html'

    def post(self, request, *args, **kwargs):
        category = Category.objects.all()
        return render(request,self.template_name,
            {'category_list': category})

class Pages(TemplateView):
    template_name = 'notes/index.html'
    form_class = NoteForm

    # def get_context_data(self, **kwargs):
    #     context = super(Pages, self).get_context_data(**kwargs)
    #     return context

    def get_context_data(self,*args, **kwargs):
        context = super(Pages, self).get_context_data(*args, **kwargs)
        context.update(NoteForm=NoteForm())
        return context








var noteApp = angular.module('noteApp', [
	'ui.router'
	,'ngRoute'
	,'ngResource'
	, 'restangular'
	, 'ngFileUpload'
	, 'pascalprecht.translate'
	, 'ngCookies'
	, 'ui.bootstrap'
	,'autoActive'
]);

/ Get list of categories from rest framework
noteApp.factory('NoteService', function($resource){
	return $resource('http://localhost:8000/api/note/');
});

// List of categories
productsApp.controller('NoteController',
	function ($scope, NoteService) {
		NoteService.query(function(result) {
			$scope.noteList=result;
		});
});
